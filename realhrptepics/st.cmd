#!/usr/local/bin/iocsh

require sinq,koennecke

epicsEnvSet("TOP","/opt/hrpt/epics")

#---------- connect to EL734 controllers
drvAsynIPPortConfigure("serial1", "hrpt-ts:3004",0,0,0)
EL734CreateController("mota","serial1",12);

drvAsynIPPortConfigure("serial2", "hrpt-ts:3005",0,0,0)
#drvAsynIPPortConfigure("serial2", "localhost:8080",0,0,0)
EL734CreateController("motb","serial2",7);

drvAsynIPPortConfigure("serial3", "hrpt-ts:3002",0,0,0)
#drvAsynIPPortConfigure("serial3", "localhost:8080",0,0,0)
EL734CreateController("motc","serial3",6);

drvAsynIPPortConfigure("serial4", "hrpt-ts:3003",0,0,0)
EL734CreateController("motd","serial4",2);

#-------------- connect to Delta Tau
pmacAsynIPConfigure("mcu1","hrpt-mcu1:1025")
#pmacAsynIPConfigure("mcu1","localhost:8080")
pmacCreateController("mote","mcu1",0,8,50,10000);
pmacCreateHRPTAxis("mote",1);
pmacCreateHRPTAxis("mote",2);
pmacCreateHRPTAxis("mote",3);
pmacCreateHRPTAxis("mote",4);



### Motors

dbLoadRecords("$(TOP)/db/asynRecord.db","P=SQ:HRPT:,R=serial1,PORT=serial1,ADDR=0,OMAX=80,IMAX=80")
dbLoadTemplate "$(TOP)/mota.substitutions"

dbLoadRecords("$(TOP)/db/asynRecord.db","P=SQ:HRPT:,R=serial2,PORT=serial2,ADDR=0,OMAX=80,IMAX=80")
dbLoadTemplate "$(TOP)/motb.substitutions"

dbLoadRecords("$(TOP)/db/asynRecord.db","P=SQ:HRPT:,R=serial3,PORT=serial3,ADDR=0,OMAX=80,IMAX=80")
dbLoadTemplate "$(TOP)/motc.substitutions"

dbLoadRecords("$(TOP)/db/asynRecord.db","P=SQ:HRPT:,R=serial4,PORT=serial4,ADDR=0,OMAX=80,IMAX=80")
dbLoadTemplate "$(TOP)/motd.substitutions"

dbLoadRecords("$(TOP)/db/asynRecord.db","P=SQ:HRPT:,R=mcu1,PORT=mcu1,ADDR=0,OMAX=80,IMAX=80")
dbLoadTemplate "$(TOP)/mote.substitutions"

#--------- load EL737 counter box
drvAsynIPPortConfigure("cter1","hrpt-ts:3006",0,0,0)
dbLoadRecords("$(TOP)/db/asynRecord.db","P=SQ:HRPT:,R=cter1,PORT=cter1,ADDR=0,OMAX=80,IMAX=80")
dbLoadRecords("${TOP}/db/el737Record.db","P=SQ:HRPT:counter,PORT=cter1,DESC=HRPTCounter")

#asynSetTraceIOMask("cter1",0,2)


#-------------- load SPS
drvAsynIPPortConfigure("sps1", "hrpt-ts:3008",0,0,0)
#drvAsynIPPortConfigure("sps1", "localhost:8080",0,0,0)
dbLoadRecords("$(TOP)/db/asynRecord.db","P=SQ:HRPT:,R=spsdirect,PORT=sps1,ADDR=0,OMAX=80,IMAX=80")
dbLoadRecords("$(TOP)/db/spshrpt.db","PREFIX=SQ:HRPT:SPS1:")


iocInit

## Start any sequence programs
#seq sncxxx,"user=koenneckeHost"
