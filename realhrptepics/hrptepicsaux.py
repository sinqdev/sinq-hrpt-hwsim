#!/usr/bin/env python
"""
  EPICS Drivers for the HRPT lmd400 device and the radial collimator controller at HRPT.
  This is the real thing...

  Mark Koennecke, January 2019
"""
from pcaspy import Driver, SimpleServer
import random
import pdb
from sinqcomlib.BaseProtocoll import BaseProtocoll
from sinqcomlib.ProcessQueue import ProcessQueue, Translator, IOTransaction
from sinqcomlib.PCAspyDriver import PCASpyDriver
import time
from threading import Thread

#--------------- LDMD400 stuff

class ProcessLMD400:
    def __init__(self,proto, callback):
        self.protocol = proto
        self.valueCallback = callback
        self.lineCount = 0
        self.values = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19]

    def storedata(self,start,line):
        data = line.split()
        for i in range(1,len(data)):
            val = data[i]
            if val.find('noinp') >= 0 or val.find('----') >= 0:
                val = 99.99
            else:
                val = val.replace(',','.')
                val = val.strip('() ')
            self.values[start+i-1] = float(val)

    def setValueCallback(self,valCallback):
        self.valueCallback = valCallback

    """
       This thing does not accept any commands but at regular intervals sends either 
       alarm messages or data messages with various readings. This code handles this. 
    """
    def process(self):
        line = self.protocol.getResponse()
        if line:
            if line.find('Alarm') >= 0 or line.find('Pre') >= 0:
                transaction = IOTransaction('ALARM',line)
                transaction.status = 'OK'
                self.valueCallback.updateValue(transaction)
            elif line.find('001...008') >= 0:
                self.storedata(0,line)
            elif line.find('009...016') >= 0:
                self.storedata(8,line)
            elif line.find('017...020') >= 0:
                self.storedata(16,line)
                transaction = IOTransaction('VALUES',self.values)
                transaction.status = 'OK'
                self.valueCallback.updateValue(transaction)

#------------------------------------------------
lmdpvdb = {
    'ALARM' : {
        'type' : 'char',
        'count' :256,
        'value': 'I am not alarmed',
        'scan' : 0,
    },
    'VALUES' : {
        'count' : 20,
        'scan'  : 0,
    }
}

#----------------- Radial Collimator stuff

radicoldb = {
    'RUN' : {
        'type': 'int',
        'asyn' : True,
    },
    'RUNRBV' : {
        'type' : 'int', 
        'scan': 10,
    },
}

class RadialTranslator(Translator):
    def encode(self,item):
        if item.key == 'RUN':
            if item.value == 1:
                return "exec init,0"
            else:
                return "sstp 1"
        elif item.key == 'RUNRBV':
            return 'print bsy'
        else:
            raise KeyError()

    def decode(self,item,response):
        if item.key == 'RUNRBV' or item.key == 'RUN':
            if response.find('TRUE')>= 0  or response.find('6002') >=0:
                item.value = 1
            else:
                item.value = 0
        else:
            raise KeyError()

class RadialQueue(ProcessQueue):
    def __init__(self,transe,proto,valCallback, timeout):
        ProcessQueue.__init__(self,transe,proto,valCallback,timeout)
        self.finishtime = -1
        self.buffer = ""

    """
       The radial collimator controller sends copious amounts of text without proper
       termination. Siiiggghh! The way to handle this is to append everything to a 
       buffer until the timeout has been reached. 
    """
    def process(self):
        if self.transaction:
            try:
                if time.time() < self.transaction.timeout:
                    if self.protocol.isReadable():
                        self.buffer = self.buffer + self.protocol.getResponse()
                    else:
                        return
                else:
 #                   print('The RADI sent: >> ' + self.buffer + ' <<')
                    self.translator.decode(self.transaction,self.buffer)
                    self.transaction.status = 'OK'
                    self.valueCallback.updateValue(self.transaction)
                    if self.transaction.key == 'RUNRBV':
                        self.transaction.key = 'RUN'
                        self.valueCallback.updateValue(self.transaction)
                    self.transaction = None
                    self.buffer = ''
                    return
            except Exception as e:
                self.transaction.status = 'Error'
                self.transaction.value = str(e)
                self.valueCallback.updateValue(self.transaction)
                self.transaction = None
                return
        else:
            if self.isEmpty():
                return
            else:
                self.transaction = self.dequeue()
                self.buffer = ""
                self.transaction.timeout = time.time() + self.timeout
                data = self.translator.encode(self.transaction)
#                print('Sending >> %s  << to RADI' %(data))
                self.protocol.write(data)
        

def runDeviceIO():
    while True:
        lmdproc.process()
        radiproc.process()
        time.sleep(0.2)

if __name__ == '__main__':
    server = SimpleServer()
    server.createPV( 'SQ:HRPT:lmd400:', lmdpvdb)
    server.createPV( 'SQ:HRPT:radicol:', radicoldb)


    lmdprot = BaseProtocoll('hrpt-ts',3012)
    lmdprot.connect()
    lmdproc = ProcessLMD400(lmdprot,None) 

   
    trans = RadialTranslator()
    radiprot = BaseProtocoll('hrpt-ts',3013)
    radiprot.connect()
    radiproc = RadialQueue(trans,radiprot,None,3)

    driver = PCASpyDriver(radiproc,['RUNRBV'],['RUN'])
    radiproc.setValueCallback(driver);
    lmdproc.setValueCallback(driver)
    driver.setParam('ALARM','No Alarm')
    driver.setParam('VALUES',[0.,1.,2.,3.,4.,5.,6.,7.,8.,9.,10.,11.,12.,13.,14.,15.,16.,17.,18.,19.])
    driver.updatePVs()


    # process CA transactions and device IO
    t = Thread(target=runDeviceIO)
    t.start()
    while True:
        server.process(0.2)
#        lmdproc.process()
#        radiproc.process()
