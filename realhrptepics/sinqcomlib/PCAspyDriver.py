"""
   This is a standard Driver subclass to adapt between PCAspy and this IO system

   Mark Koennecke, January 2019
"""
from sinqcomlib.ProcessQueue import ProcessQueue, IOTransaction, IOCallback
from pcaspy import Driver, Alarm, Severity


class PCASpyDriver(Driver,IOCallback):
    def __init__(self,queue,readlist,writelist):
        super(PCASpyDriver,self).__init__()
        self.queue = queue
        self.readlist = readlist
        self.writelist = writelist

    def read(self,reason):
        if reason in self.readlist and not self.queue.isQueued(reason):
            transaction = IOTransaction(reason,None)
#            print('Enqueing read for ' + reason)
            self.queue.enqueue(transaction)
        return Driver.read(self,reason)

    def write(self,reason,value):
        if reason in self.writelist:
            transaction = IOTransaction(reason,value)
            self.queue.enqueuePriority(transaction)
            return True
        else:
            return False

    def updateValue(self,transaction):
        if transaction.status == 'OK':
#            print('Updating %s to %s' %(transaction.key,str(transaction.value)))
            self.setParam(transaction.key,transaction.value)
            self.setParamStatus(transaction.key,Alarm.NO_ALARM,Severity.NO_ALARM)
        else:
            if transaction.value == 'Timeout':
                self.setParamStatus(transaction.key,Alarm.TIMEOUT_ALARM,Severity.MAJOR_ALARM)
            else:
                self.setParamStatus(transaction.key,Alarm.COMM_ALARM,Severity.MAJOR_ALARM)
            print('Error %s on key %s' %(transaction.value,transaction.key))
        self.updatePVs()
