"""
     This file contains the I/O processing code. The main driver is the ProcessQueue class. 
     It uses the IOTransaction class to hold data. It also uses a subclass of Translator 
     for translating IOTransactions to device dependent commands. The Translator is also 
     responsible for decoding responses. 

     Mark Koennecke, January 2019 
"""
import time

class Translator:
    """
       Return the device specific command for the key and value in the 
       item. Item is a IOTransaction
    """
    def encode(self,item):
        pass
    """
      Decode the response from the device and update item with the response value.
      Raise an exception on errors
    """
    def decode(self,item,reponse):
        pass
#-----------------------------------------------------------------------------------------
class IOTransaction:
    """
      A read request would have a value of None
    """
    def __init__(self,key,value):
        self.key = key
        self.value = value
        self.status = None
#---------------------------------------------------------------------------------------
class IOCallback:
    def updateValue(self,transaction):
        pass
#----------------------------------------------------------------------------------------
class ProcessQueue:
    """
      transe is a Translator for converting keys into device commands
      proto is an instance of Baseprotocoll for talking to the device
      valCallback is a callback function to call when a response has been received  
    """
    def __init__(self,transe,proto,valCallback, timeout):
        self.items = []
        self.transaction = None
        self.translator = transe
        self.protocol = proto
        self.valueCallback = valCallback
        self.timeout = timeout

    def setValueCallback(self,valCallback):
        self.valueCallback = valCallback

    def isEmpty(self):
        return self.items == []

    def isQueued(self,pv):
        for item in self.items:
            if item.key == pv:
                return True
        return False

    def enqueue(self, item):
        if isinstance(item,IOTransaction):
            self.items.insert(0,item)
        else:
            raise TypeError()

    def enqueuePriority(self,item):
        if isinstance(item,IOTransaction):
            self.items.append(item)
        else:
            raise TypeError()


    def dequeue(self):
        return self.items.pop()

    def size(self):
        return len(self.items)

    """
      Override this function if you need device specific processing.

      This function is supposed to be called repeatedly from some form of main loop 
    """
    def process(self):
        if self.transaction:
            try:
                response = protocol.getResponse()
                if response:
                    translator.decode(self.transaction,response)
                    self.transaction.status = 'OK'
                    self.valueCallback.updateValue(self.transaction)
                    self.transaction = None
                    return
                else:
                    if time.time() > self.transaction.timeout:
                        self.transaction.status = 'Error'
                        self.transaction.value = 'Timeout'
                        self.valueCallback.updateValue(self.transaction)
                        self.transaction = None
                    else:
                        return
            except Exception as e:
                self.transaction.status = 'Error'
                self.transaction.value = str(e)
                self.valueCallback.updateValue(self.transaction)
                self.transaction = None
                return
        else:
            if self.empty():
                return
            else:
                self.transaction = self.dequeue()
                self.transaction.timeout = time.time() + self.timeout
                data = self.translator.encode(self.transaction)
                self.protocol.write(data)
