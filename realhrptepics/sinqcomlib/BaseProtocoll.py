"""
This is a python class for managing the communication
with a hardware device on the network. It is both a base class and 
an implementation for the poular <CR><LF> style of protocols

This is derived of the SICS communication class in a separate package
"""
import socket, sys, select, time

class BaseProtocoll:

    def __init__(self, host, port):
        self.host = host
        self.port = port
        buffer = ''

    def connect(self):
        self.socke = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socke.connect((self.host,self.port))
        self.sofi = self.socke.makefile('r+',1)

    def disconnect(self):
        self.socke.close()

    def getline(self):
        return self.sofi.readline()

    def writeline(self, line):
        self.sofi.write(line + "\r\n")
        self.sofi.flush()

    def isReadable(self):
        write = []
        read = [self.socke]
        inn, out, ex = select.select(read,write,read,.1)
        if self.socke in inn:
            return True
        else:
            return False
           
    def clearBuffer(self):
        self.socke.setblocking(0)
        self.buffer = ''
    
    def readIntoBuffer(self,timeout):
        start = time.time()
        self.buffer = ''
        while time.time() < start + timeout:
            if self.isReadable():
                read = True
                while read:
                    try:
                        self.buffer += self.getline()
                    except:
                        read = False
        self.socke.setblocking(1)
        return self.buffer

    """
      This is one of the methods to override for a different protocol. 
      It just writes the encoded data to the network in a way appropriate to 
      the protocol.  
    """
    def write(self,data):
        self.writeline(data)

    """
       This is the other method to override for other protocols. It tries to read 
       a response. It is supposed not to block but rather return None when no 
       response is available. 
    """
    def getResponse(self):
        if self.isReadable():
            return self.getline()
        else:
            return None

        
