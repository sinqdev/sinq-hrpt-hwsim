# vim: ft=dockerfile
# docker build --rm --tag=local/c7-base
#   --build-arg HTTP_PROXY=${http_proxy}
#   --file=Dockerfile_base .
FROM centos:7

ARG HTTP_PROXY

COPY epics /opt/software/epics
COPY hrpt   /opt/hrpt

#-------------- motor controller and counter
#EXPOSE 60001
#EXPOSE 62000

#------------- EPICS IOC
EXPOSE 5064/tcp
EXPOSE 5064/udp
#EXPOSE 5065/udp
EXPOSE 5065/tcp
## The pcaSpy IOC
EXPOSE 5080/udp
EXPOSE 5080/tcp

#-------------- sinqhm
Expose 8080/tcp

RUN sed -i /etc/yum.conf -e '/^proxy/d'; \
      yum -y install epel-release; \
      yum install -y telnet nc net-tools tmux which tcl mxml python-pip gcc g++ gcc-c++;\
      yum install -y python-twisted-core swig python-devel python-wheel readline-devel; \
      /opt/software/epics/installepics; \
      pip install --upgrade pip; \
      /opt/software/epics/installpcaspy; \
      rm -rdf /opt/software/epics/*; 


CMD  ["/opt/hrpt/simfiles/runhrptsim"]




