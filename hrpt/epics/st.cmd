#!/usr/local/bin/iocsh

require sinq,koennecke

epicsEnvSet("TOP","/opt/hrpt/epics")

#---------- connect to controllers
drvAsynIPPortConfigure("serial1", "localhost:60001",0,0,0)
EL734CreateController("mota","serial1",12);

drvAsynIPPortConfigure("serial2", "localhost:60002",0,0,0)
EL734CreateController("motb","serial2",7);

drvAsynIPPortConfigure("serial3", "localhost:60003",0,0,0)
EL734CreateController("motc","serial3",5);

drvAsynIPPortConfigure("serial4", "localhost:60004",0,0,0)
EL734CreateController("motd","serial4",2);

drvAsynIPPortConfigure("serial5", "localhost:60005",0,0,0)
EL734CreateController("mote","serial5",2);

### Motors

dbLoadRecords("$(TOP)/db/asynRecord.db","P=SQ:HRPT:,R=serial1,PORT=serial1,ADDR=0,OMAX=80,IMAX=80")
dbLoadTemplate "$(TOP)/mota.substitutions"

dbLoadRecords("$(TOP)/db/asynRecord.db","P=SQ:HRPT:,R=serial2,PORT=serial2,ADDR=0,OMAX=80,IMAX=80")
dbLoadTemplate "$(TOP)/motb.substitutions"

dbLoadRecords("$(TOP)/db/asynRecord.db","P=SQ:HRPT:,R=serial3,PORT=serial3,ADDR=0,OMAX=80,IMAX=80")
dbLoadTemplate "$(TOP)/motc.substitutions"

dbLoadRecords("$(TOP)/db/asynRecord.db","P=SQ:HRPT:,R=serial4,PORT=serial4,ADDR=0,OMAX=80,IMAX=80")
dbLoadTemplate "$(TOP)/motd.substitutions"

dbLoadRecords("$(TOP)/db/asynRecord.db","P=SQ:HRPT:,R=serial5,PORT=serial5,ADDR=0,OMAX=80,IMAX=80")
dbLoadTemplate "$(TOP)/mote.substitutions"


#--------- load EL737 counter box
drvAsynIPPortConfigure("cter1","localhost:62000",0,0,0)
dbLoadRecords("$(TOP)/db/asynRecord.db","P=SQ:HRPT:,R=cter1,PORT=cter1,ADDR=0,OMAX=80,IMAX=80")
dbLoadRecords("${TOP}/db/el737Record.db","P=SQ:HRPT:counter,PORT=cter1,DESC=HRPTCounter")

#asynSetTraceIOMask("cter1",0,2)

iocInit

## Start any sequence programs
#seq sncxxx,"user=koenneckeHost"
