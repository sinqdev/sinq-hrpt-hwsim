#!/usr/bin/env python
"""
  Drivers for the HRPT lmd400 device and the radial collimator controller at HRPT.
  This is the simulation version

  Mark Koennecke, January 2019
"""
from pcaspy import Driver, SimpleServer
import sys
import time
import random
import pdb
import threading


lmdpvdb = {
    'ALARM' : {
        'type' : 'char',
        'count' :256,
        'value': 'I am not alarmed',
    },
    'VALUES' : {
        'count' : 32,
    }
}

radicoldb = {
    'RUN' : {
        'type': 'int',
        'asyn' : True,
    },
    'RUNRBV' : {
        'type' : 'int', 
    },
}

"""
  The simulation of the lmd400 provides new random values any 10 seconds for values 
  and an alarm every 120 seconds
"""
class SimLMD400:
    lastValUpdate = time.time()
    lastAlarmUpdate = time.time()
    alarmCount = 0

    def runSimulation(self,driver):
        if time.time() > self.lastValUpdate + 10:
            self.lastValUpdate = time.time()
            v = []
            for i in range(32):
                v.append(random.random()*44.)
            driver.setParam('VALUES',v)
            driver.updatePVs()
        if time.time() > self.lastAlarmUpdate + 120:
            self.lastAlarmUpdate = time.time()
            self.alarmCount = self.alarmCount + 1
            driver.setParam('ALARM', 'I feel %d alarmed' % (self.alarmCount))
            driver.updatePVs()
                

class HRPTDriver(Driver):
    def  __init__(self):
        super(HRPTDriver, self).__init__()
        self.tid = None

    def write(self,reason,value):
        if reason == 'RUN':
            if not self.tid:
                self.tid = threading.Thread(target=self.runRCOLL,args=(value,))
                self.tid.start()
                self.setParam('RUN',value);
                self.updatePVs()
                return True
            else:
                return False
        return False

    def runRCOLL(self,value):
        time.sleep(5)
        self.setParam('RUNRBV',value);
        self.updatePVs()
        self.tid = None
   


if __name__ == '__main__':
    server = SimpleServer()
    server.createPV( 'SQ:HRPT:lmd400:', lmdpvdb)
    server.createPV( 'SQ:HRPT:radicol:', radicoldb)
    driver = HRPTDriver()
    sld = SimLMD400()

    # process CA transactions
    while True:
        server.process(0.1)
        sld.runSimulation(driver)
