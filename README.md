# SINQ-HRPT-HWSIM

This is a deep simulation of the instrument hardware of the SINQ instrument HRPT 
in a docker container. This contains:

- A simulation of the EL734 motor controllers of HRPT
- A simulation of the EL737s counter box used at HRPT
- EPICS support for both of the above
- An instance of SinqHM 
- A pcaSpy simulation of the radial collimator and the lmd400 temperature logger which not only simulates 
  but also provides an EPICS interface


This repository is only required if you need to rebuild the container with the 
simulation. The resulting container is available to docker at mkoennecke/sinq:hrpt-sim


This repository not only contains the HRPT HW simulation but also a docker container for running the HRPT IOC's on the 
real thing. This alos includes the stuff to run the HRPT IOC's on bare metal. See below, in the HRPT operations section for more 
information.

## Running

The container can be run like any other docker container. But in order to to get at EPICS 
and SinqHM you need to expose some ports. See file **runhrptsimepics** for details. This file 
works for docker on RHEL-7. 

On OSX, there is some problem with port 5065 which is used by EPICS for beacons. See file 
**runhrptsimosx** for details. There may be beacon related EPICS messages to ignore...


In order for EPICS clients to locate the PV's in the container, EPICs CA variables have to be set up in a special way:

- EPICS_CA_AUTO_ADDR_LIST=NO
- EPICS_CA_ADDR_LIST=127.0.0.1:5064 127.0.0.1:5080

This works both for linux and OSX.  

### Running from dockerhub

The simulation can be run from dockerhub through the command:

    docker run -d --name=hrptsim --net host -p 5064:5064/tcp -p 5064:5064/udp -p 5065:5065/udp -p 5065:5065/tcp -p 5080:5080/tcp -p 5080:5080/udp -p 8080:8080 mkoennecke/sinq:hrpt-epics /opt/hrpt/simfiles/runhrptsim

## Inventory

SinqHM is exported to port 8080, EPICS to the normal EPICS ports. The prefix for HRPT is
SQ:HRPT:


HRPT has two monochromators on a monochromator lift. Each monochromator can be translated in two directions and tilted 
in two directions. And a theta rotation each, too. This gives rise to quite a zoo of motors:

- SQ:HRPT:mota:MTVL, lower monochromator upper translation 
- SQ:HRPT:mota:MGPL, lower monochromator lower tilt
- SQ:HRPT:mota:MGVL, lower monochromator upper tilt
- SQ:HRPT:mota:MCVL, lower monochromator curvature
- SQ:HRPT:mota:MGVU, upper monochromator upper tilt
- SQ:HRPT:mota:MCVU, upper monochromator curvature
- SQ:HRPT:mota:MOML, lower monochromator theta motor
- SQ:HRPT:mota:MTPL, lower monochromator lower translation
- SQ:HRPT:mota:MOMU, upper monochromator theta motor
- SQ:HRPT:mota:MTPU, upper monochromator lower translation
- SQ:HRPT:mota:MTVU, upper monochromator upper translation
- SQ:HRPT:mota:MGPU, upper monochromator lower tilt

- SQ:HRPT:motb:MEXZ, monochromator lift
- SQ:HRPT:motb:D1R, D1 slit right blade
- SQ:HRPT:motb:D1L, D2 slit left blade

- SQ:HRPT:motc:STT, two theta detector
- SQ:HRPT:motc:SOM, sample rotation
- SQ:HRPT:motc:CHPOS, sample changer position
- SQ:HRPT:motc:STX, sample x translation
- SQ:HRPT:motc:STY, sample y translation

- SQ:HRPT:motd:CEX1, collimator cylinder 1 rotation
- SQ:HRPT:motd:CEX2, collimator cylinder 2 rotation

- SQ:HRPT:mote:BRLE, beam reduction slit left blade
- SQ:HRPT:mote:BRRI, beam reduction slit right blade
- SQ:HRPT:mote:BRTO, beam reduction slit top blade
- SQ:HRPT:mote:BRBO, beam reduction slit bottom blade

Counter, this is the root of a EPICS scalerRecord:
- SQ:HRPT:counter

SPS:
- SQ:HRPT:SPS1:AnalogInput
- SQ:HRPT:SPS1:DigitalInput
- SQ:HRPT:SPS1:Push

Radial collimator:
- SQ:HRPT:radicol:RUN, for switching the collimator on or off
- SQ:HRPT:radicol:RUNRBV, readback of the radial collimator status

LMD400:
- SQ:HRPT:lmd400:VALUES, detector temperature values
- SQ:HRPT:lmd400:ALARM, alarm status of the LMD400

There is direct access to the controller through the asynRecord for most of the controllers:
- SQ:HRPT:serial1:AINP, reply from controller, Motor controller mota
- SQ:HRPT:serial1:AOUT, command to controller. Motor controller mota
- SQ:HRPT:serial2:AINP, reply from controller, Motor controller motb
- SQ:HRPT:serial2:AOUT, command to controller. Motor controller motb
- SQ:HRPT:serial3:AINP, reply from controller, Motor controller motc
- SQ:HRPT:serial3:AOUT, command to controller. Motor controller motc
- SQ:HRPT:serial4:AINP, reply from controller, Motor controller motd
- SQ:HRPT:serial4:AOUT, command to controller. Motor controller motd
- SQ:HRPT:cter1:AINP, reply from controller, Counter controller
- SQ:HRPT:cter1:AOUT, command to controller, Counter controller


## Building

For building the docker container after you changed something, run script  **hrptsimbuild**

## Organisation of the Repository

The master of everything is of course the **Dockerfile**. Directory **epics** contains 
archives of the necessary EPICS software which needs to be added to vanilla rhel-7 container 
and a script which installs all the EPICS stuff.  

The directory **hrpt** contains all the HRPT related stuff:

- subdirectory **simfiles** the simulations for the HRPT hardware
- subdirectory **sinqhm** all that is needed to run  SinqHM
- subdirectory **epics** the EPICS startup and configuration files for HRPT

In the container all the HRPT stuff can be found at /opt/hrpt. Docker containers 
do not fancy services, thus the software is started up through a script runhrptsim which 
lives in /opt/hrpt/sinfiles in the container and hrpt/simfiles outside. 

## HRPT Operations

There is another docker file, Dockerfile.hrpt, which builds a container containing the EPICS IOC's for 
the real HRPT instrument, accessing the real hardware. The actual EPICS configuration and startup files reside in 
the **realhrptepics** directory. There are some scripts:

- **runhrptepics** which runs the IOC container
- **hrptbuild** for building the IOC container




 